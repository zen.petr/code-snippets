#!/bin/bash

# user should declare the file as an argument
show_usage() {
	echo "Usage: $0 missing argument - path to file" 1>&2
	exit 1
}

# main program


if [ $# -ne 1 ]; then
	show_usage
else
	if [ -f $1 ]; then
	source_file=$1
	sed -e '2,${/^[[:blank:]]*#/d;s/#.*//}' $1 > "prod.${source_file}"
	else
	echo "Invalid source file" 2>> rmErr.txt 1>&2
	show_usage
	fi
fi

printf "File to change is ${source_file}\n"

#takes only one file for now
# need to check with non regular file if error txt is created
