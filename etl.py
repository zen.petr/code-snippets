import requests
import csv

# this is where we will extract
def get_launch_data(url):
    launch_data = requests.get(url)
    return launch_data.json()

# transform
def transfer_data(payload_data):
    mission_names = []
    mission_image = []
    launch_year = []
    flight_number = []

    for launch in payload_data:
        mission_names.append(launch["mission_name"])
        mission_image.append(launch["links"]["mission_patch"])
        launch_year.append(launch["launch_year"])
        flight_number.append(launch["flight_number"])

        #Loads data to CSV
        with open('flightData.csv', 'w', newline='') as f:
            for a, b, c, d in zip(flight_number, mission_names, launch_year, mission_image):
                writer = csv.writer(f)
                writer.writerow([a, b, c, d])

        # print(len(mission_names))
        # print(len(mission_image))
        # print(len(launch_year))
        # print(len(flight_number))

result = get_launch_data('https://api.spacexdata.com/v3/launches')

transfer_data(result)