# pip3 install cs50
from cs50 import get_int

def main():
    i = check_height()
    # print(i)

    for k in range(i):
        print(" "*(i-k-1) + "#"*(k+1))
    print()

def check_height():
    while True:
        x = get_int("give number: ")
        if x >= 1 and x <= 8:
            # print("correct num")
            break
    return x

main()